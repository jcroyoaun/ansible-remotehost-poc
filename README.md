# Intro

This repo assumes this was made for a gitlab runner that was meant to run a pipeline.

### Pre-requisites
1-> the "GitLab Runner" host needs to have its ssh keys created, and added to the remote hosts.
2-> the remote hosts need to be online


## Editing the files before running.
1-> Make sure to change the references containing <> in the inventoy files and the playbook.yml with actual data for your remote hosts.


## To run this playbook:
docker run -v $(pwd):/ansible -v ~/.ssh/known_hosts:/root/.ssh/known_hosts -v ~/.ssh/id_rsa:/root/.ssh/id_rsa willhallonline/ansible:latest ansible-playbook -i inventory/remote-servers playbook.yml